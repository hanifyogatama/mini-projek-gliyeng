import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './assets/css/style.css';
import './assets/css/bootstrap.css';
import HomePage from 'pages/HomePage';
import AboutUsPage from 'pages/AboutUsPage';
import DonationPage from 'pages/DonationPage';
import NewsPage from 'pages/NewsPage';
import EditProfilePage from 'pages/EditProfilePage';
import HistoryDonationPage from 'pages/HistoryDonationPage';
import UpdateActivityPage from 'pages/UpdateActivityPage';
import LoginPage from 'pages/Login';
// import RegisterPage from 'pages/Register';


function App() {
  return (
    <div className="App">
      <Router>
        <Route exact='true' path="/" component={HomePage}></Route>
        <Route path="/tentang" component={AboutUsPage}></Route>
        <Route path="/donasi" component={DonationPage}></Route>
        <Route path="/berita" component={NewsPage}></Route>
        <Route path="/editprofile" component={EditProfilePage}></Route>
        <Route path="/historidonasi" component={HistoryDonationPage}></Route>
        <Route path="/update_activity" component={UpdateActivityPage}></Route>
        <Route path="/login" component={LoginPage}></Route>
        {/* <Route path="/register" component={RegisterPage}></Route> */}
      </Router>
    </div>
  );
}

export default App;
