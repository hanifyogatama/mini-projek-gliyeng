import React, { Component } from 'react';
import Header from "components/Header";
import SearchBar from "components/SearchBar";
import AboutUs from "components/AboutUs";
import Footer from "components/Footer";

export default class AboutUsPage extends Component {
    render() {
        return (
            <section>
                <Header {...this.props} />
                <SearchBar />
                <AboutUs />
                <Footer />
            </section>
        )
    }
}
