import React, { Component } from 'react';

import Header from "components/Header";
import SearchBar from "components/SearchBar";
import Slider from "components/Slider";
import Feature from "components/Feature";
import CounterCard from "components/CounterCard";
import DonationContent from "components/DonationContent";
import NewsContent from "components/NewsContent";
import Testimony from "components/Testimony";

import Footer from "components/Footer";

import landingPage from 'json/landingPage.json';

export default class HomePage extends Component {
    render() {
        return (
            <>
                <Header {...this.props}></Header>
                <SearchBar></SearchBar>
                <Slider></Slider>

                <h3 className="text-uppercase text-center mt-5" style={{ fontWeight: "bold" }}>kategori program donasi</h3>
                <p className="text-center"><small className="text-muted">Temukan program donasi pilihanmu di berbagai kategori</small></p>

                <Feature></Feature>
                <CounterCard></CounterCard>

                <h3 className="text-uppercase text-center" style={{ fontWeight: "bold" }}>berbagi program donasi</h3>
                <p className="text-center"><small className="text-muted">Temukan program donasi pilihanmu di berbagai program</small></p>

                <DonationContent data={landingPage.categories} />

                <h3 className="text-uppercase text-center" style={{ fontWeight: "bold" }}>berita</h3>
                <p className="text-center"><small className="text-muted">Berbagi kabar kebaikan ke seluruh</small></p>


                <NewsContent data={landingPage.categories} />

                <Testimony />
                <Footer />

            </>
        )
    }
}
