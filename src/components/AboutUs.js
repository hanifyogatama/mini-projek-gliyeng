import React from 'react';
//import AvatarIcon from 'assets/images/avatar.jpg';
import Logo from 'assets/images/underMaintenance.jpg';

export default function AboutUs(props) {
    return (
        <div className="container text-center">
            <h3 className="text-uppercase text-center mt-5" style={{ fontWeight: "bold" }}>who are we ?</h3>
            <p className="text-center"><small className="text-muted">lorem lorem lorem lorem lorem
            </small></p>
            <a className="navbar-brand" href="">
                <img
                    src={Logo}
                />
            </a>
        </div>
    )
}
