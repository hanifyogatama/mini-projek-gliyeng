import React from 'react';
//import AvatarIcon from 'assets/images/avatar.jpg';
import Logo from 'assets/images/logo.svg';
import Login from '../pages/Login';
export default function Header(props) {

    const getNavLinkClass = path => {
        return props.location.pathname === path ? " active" : ""
    }

    return (
        <div className="container">
            <nav className="navbar navbar-expand-lg navbar-light px-0 ">
                <a className="navbar-brand" href="/">
                    <img
                        src={Logo}
                    />
                </a>

                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto ">
                        <li className={`nav-item${getNavLinkClass("/")} px-3`}>
                            <a className="nav-link py-1" href="/">Beranda</a>
                        </li>
                        <li className={`nav-item${getNavLinkClass("/tentang")} px-3`}>
                            <a className="nav-link py-1" href="/tentang">Tentang Kami</a>
                        </li>
                        <li className={`nav-item${getNavLinkClass("/donasi")} px-3`}>
                            <a className="nav-link py-1" href="/donasi">Program Donasi</a>
                        </li>
                        <li className={`nav-item${getNavLinkClass("/berita")} px-3`}>
                            <a className="nav-link py-1" href="/berita">Berita</a>
                        </li>


                        {/* button login dan sign in */}
                        <div>
                            <a className=" py-0" href="/login">
                                <button className="btn btn-account-1 mx-3" type="submit" href="/login" style={{ fontSize: 12 }}>MASUK</button>
                            </a>
                        </div>

                        <div>
                            <a className=" py-0" href="/register">
                                <button className="btn btn-account" type="submit" href="/register" style={{ fontSize: 12 }}>MASUK</button>
                            </a>
                        </div>
                        {/* untuk kalo user udah login */}

                        {/* <div>
                            <a className="nav-link" href="/">Kenobi</a>
                        </div>
                        <li>
                            <img
                                width="36"
                                height="36"
                                src={AvatarIcon}
                                className="rounded-circle"
                            />
                        </li> */}

                    </ul>
                </div>
            </nav>
        </div>

    )
}
