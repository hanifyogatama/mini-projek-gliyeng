import React from 'react';
import data from '../data';
import Karsa from 'assets/images/oven.svg';
import Ilmu from 'assets/images/book.svg';
import Rasa from 'assets/images/chat.svg';

export default function Feature() {

    return (
        <section>
            <div className="container mt-3 mb-5" >
                <div class="row text-center">
                    <div class="col-sm-4">
                        <div class="card-feature" style={{ height: 357, width: 360 }}>
                            <div class=" card-body ">
                                <img className="mb-5" src={Rasa} />
                                <h5 class="card-title">Sambung Rasa</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card-feature" style={{ height: 357, width: 360 }}>
                            <div class="card-body">
                                <img className="mb-5" src={Karsa} />
                                <h5 class="card-title">Sambung Karsa</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card-feature" style={{ height: 357, width: 360 }}>
                            <div class="card-body">
                                <img className="mb-5" src={Ilmu} />
                                <h5 class="card-title ">Sambung Ilmu</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
