import React from 'react';
import data from '../data';

export default function Slider() {

    return (
        <se>
            <h3 className=" text-center mt-5" style={{ fontWeight: "bold" }}>Mulai berdonasi dengan Ketjilbergerak untuk membantu sesama</h3>
            <div className="container mt-3 mb-4">
                <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel" >
                    <ol className="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div className="carousel-inner" style={{ height: 360 }}>

                        <div className="carousel-item active">
                            <div className="card  text-white">
                                <img className="card-img" src="https://www.paramount-land.com/wp-content/uploads/2017/03/Dummy-Header.jpg" alt="Card image" />
                                <div className="card-img-overlay">
                                    <h5 className="card-title">Card title</h5>
                                    <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                    <p className="card-text">Last updated 3 mins ago</p>
                                    <a href="#" class="btn btn-info">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                        <div className="carousel-item">
                            <div className="card  text-white">
                                <img className="card-img" src="https://www.paramount-land.com/wp-content/uploads/2017/03/Dummy-Header.jpg" alt="Card image" />
                                <div className="card-img-overlay">
                                    <h5 className="card-title">Card title</h5>
                                    <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                    <p className="card-text">Last updated 3 mins ago</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                        <div className="carousel-item">
                            <div className="card  text-white">
                                <img className="card-img" src="https://www.paramount-land.com/wp-content/uploads/2017/03/Dummy-Header.jpg" alt="Card image" />
                                <div className="card-img-overlay">
                                    <h5 className="card-title">Card title</h5>
                                    <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                    <p className="card-text">Last updated 3 mins ago</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </se>
    )
}
