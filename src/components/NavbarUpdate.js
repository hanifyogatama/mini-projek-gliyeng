import React from 'react';


export default function NavbarUpdate(props) {

    const getNavLinkClass = path => {
        return props.location.pathname === path ? " active" : ""
    }

    return (
        <div className="container mt-5">
            <nav className="navbar bg-light px-0">
                <ul className="nav-content">
                    <li className={`nav-item${getNavLinkClass("/deskripsi")}`}>
                        <a className="nav-link px-0" href="/deskripsi">Deskripsi</a>
                    </li>
                    <li className={`nav-item${getNavLinkClass("/update_activity")} pl-4 pr-3`}>
                        <a className="nav-link px-0" href="/update_activity">Update Aktivitas</a>
                    </li>
                    <li className={`nav-item${getNavLinkClass("/update_donasi")} pl-2`}>
                        <a className="nav-link px-0" href="/update_donasi">Update Donasi</a>
                    </li>
                </ul>
            </nav>
        </div >
    )
}
